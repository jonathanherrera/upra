new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: [2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017],
    datasets: [{ 
        data: [0.0,0.0039,-0.0995,0.1657,0.0424,-0.1701,-0.0656,0.0086,0.1471,0.0564,-0.1004,0.0557,-0.064,0.052,-0.1485,0.236,0.2311,0.0412],
        label: "Tasa de variación",
        borderColor: "#3e95cd",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});



